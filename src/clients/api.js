const getDataFromApi = async () => {
    const url = `../data/sample-data.json`;
    const data = await fetch(url);
    const json = await data.json();
    return json;
};

export const getCompanyInfoFromApi = async () => {
    const data = await getDataFromApi();
    return data.companyInfo;
};

export const getEmployeesFromApi = async () => {
    const data = await getDataFromApi();
    return data.employees;
};