import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
    cursor: pointer;
    display: flex;
    flex-grow: 1;
    margin: 1%;
    width: 100%;

    @media (min-width: 768px) {
        width: 48%;
    }

    @media (min-width: 1024px) {
        width: 31%;
    }
`

const Avatar = styled.div`
    background-image: url(${props => props.img});
    background-size: cover;
    background-position: center;
    min-width: 30%;
`

const Content = styled.div`
    background: ${props => props.active ? 'orange' : 'lightgrey'};
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    padding: 10px;
`

const Name = styled.h4`

`

const Bio = styled.div`

`

const Card = ({
    active,
    avatar,
    bio,
    firstName,
    lastName,
    showProfile
}) => (
    <Wrapper onClick={showProfile}>
        <Avatar img={avatar} />
        <Content active={active}>
            <Name>{firstName} {lastName}</Name>
            <Bio>{bio.length > 120 ? `${bio.substr(0, 120)}...` : bio}</Bio>
        </Content>
    </Wrapper>
);

export default Card;