import React from 'react';
import { useSelector } from 'react-redux';
import { getEmployeeById } from '../../redux/selectors';
import Card from './Card';

const CardContainer = ({
    employee: {
        avatar,
        bio,
        firstName,
        id,
        lastName
    },
    showProfile
}) => {

    const selectedEmployee = useSelector(getEmployeeById);
    const handleClick = () => showProfile(id);

    return (
        <Card 
            active={id === (selectedEmployee && selectedEmployee.id)}
            avatar={avatar}
            bio={bio}
            firstName={firstName}
            lastName={lastName}
            showProfile={handleClick}
        />
    );
}

export default CardContainer;