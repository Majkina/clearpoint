# CODE EXERCISE NOTES:

To perform the exercise up to this point took me about 4 hours. I used CRA with redux for state handling. For styling purposes I chose styled components to encapsule styles and markup on the component level, and I took mobile first approach while designing for various devices. I'm aware that not all components are exactly mirroring provided wireframes however I believe it will provide you with sufficient idea of how I would approach similar problems. All componentns have been split into container and presentational ones to separate logic from view. 

Please see the notes below covering my thoughts during the development process and how would application be designed should it scale to a larger level. 

`clients`:
- in a larger application api.js file would be split by the client (company, employee...) and named accordingly, for our purposes only one file containing all clients has been created

`components/EmployeeModal`
- this component is an employee specific modal, in a larger application this would be a generic component designed to be reused by the whole application where we would pass the content to the modal as a separate component

`redux`
- a standalone redux folder has been created with all related files within, in a larger scale application each file (actions, reducers, selectors) would be split by a component and put into relevant components redux folder, we can see the separation in those files happening already yet for the size of this application splitting them into separate files / folders wasn't necessary

`redux/actions`
- catching errors is not currently handled on user facing level, in a real life application a modal with user friendly error message would be displayed

## Next steps:
If I was going to spend more time on this application, I would make sure all props are being type checked using propTypes and I would also ensure good test coverage.

Feel free to reach out if you have any further questions.