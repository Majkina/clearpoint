export const getCompanyInfoFromState = state => state.companyInfo.data;
export const isCompanyLoading = state => state.companyInfo.loading;

export const getEmployeesFromState = state => state.employees.data;
export const isEmployeesLoading = state => state.employees.loading;

export const isEmployeeModalOpen = state => state.employeeModal.isOpen;
export const getEmployeeById = state => state.employees.data.find(employee => employee.id === state.employeeModal.id)