import React, { useEffect, useRef } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
    getEmployeeById,
    isEmployeeModalOpen
} from '../../redux/selectors';
import { closeEmployeeModal } from '../../redux/actions';
import EmployeeModal from './EmployeeModal';

const ModalContainer = () => {
    
    const ref = useRef();
    const dispatch = useDispatch();
    const employee = useSelector(getEmployeeById);
    const showModal = useSelector(isEmployeeModalOpen);

    const handleClickAway = e => {
        if (ref.current && !ref.current.contains(e.target)) {
            dispatch(closeEmployeeModal())
        }
    }

    const closeModal = () => {
        dispatch(closeEmployeeModal())
    }

    useEffect(() => {
        window.addEventListener('mousedown', handleClickAway)
        return () => {
            window.removeEventListener('mousedown', handleClickAway)
        }
    }, [])

    return showModal && (
        <EmployeeModal
            closeModal={closeModal}
            employee={employee}
            ref={ref}
        />
    );
}

export default ModalContainer;