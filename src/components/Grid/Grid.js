import React from 'react';
import styled from 'styled-components';
import CardContainer from '../Card/CardContainer';

const Wrapper = styled.div`
    padding: 25px;
`

const Content = styled.div`
    display: flex;
    flex-wrap: wrap;
`

const Header = styled.h3`
    border-bottom: 1px solid black;
`

const Grid = ({ data, isLoading, showProfile }) => (
    <Wrapper>
        <Header>Our Employees</Header>
        {isLoading ?
            <div>loading...</div> 
            : <Content>
                {data.map(employee => (
                    <CardContainer
                        employee={employee}
                        key={employee.id}
                        showProfile={showProfile}
                    />
                ))}
            </Content>

        }   
    </Wrapper>
);

export default Grid;