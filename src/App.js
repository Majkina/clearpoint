import React from 'react';
import { Provider } from 'react-redux';
import store from './redux/store';
import TopBarContainer from './components/TopBar/TopBarContainer';
import GridContainer from './components/Grid/GridContainer';

import 'normalize.css';
import EmployeeModalContainer from './components/EmployeeModal/EmployeeModalContainer';

function App() {
  return (
    <Provider store={store}>
      <EmployeeModalContainer />
      <TopBarContainer />
      <GridContainer />
    </Provider>
  );
}

export default App;
