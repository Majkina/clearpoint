import React, { forwardRef } from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
    background: rgba(0, 0, 0, 0.7);
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
`

const CloseButton = styled.div`
    cursor: pointer;
    height: 20px;
    position: absolute;
    right: 20px;
    top: 20px;
    width: 20px;

    &:after, &:before {
        background: #000000;
        content: '';
        height: 5px;
        position: absolute;
        top: 5px;
        width: 20px;
    }

    &:after {
        transform: rotate(45deg);
    }

    &:before {
        transform: rotate(-45deg);
    }
`

const Content = styled.div`
    align-conte: space-around;
    background: #ffffff;
    display: flex;
    flex-direction: column;
    left: 50%;
    height: 60%;
    padding: 25px;
    position: fixed;
    top: 50%;
    transform: translate(-50%, -50%);
    width: 60%;

    @media (min-width: 1024px) {
        flex-direction: row;
        flex-wrap: wrap;
    }
`

const Avatar = styled.div`
    background-image: url(${props => props.img});
    background-size: cover;
    background-position: center;
    height: 30%;
    margin: 10px;

    @media (min-width: 1024px) {
        flex: 1 1 40%;
        height: auto;
    }
`

const Info = styled.div`
    margin: 10px;

    @media (min-width: 1024px) {
        flex: 1 1 40%;
    }
`

const FullName = styled.h3`
    border-bottom: 2px solid grey;
    margin: 10px;

    @media (min-width: 1024px) {
        align-self: flex-end;
        flex: 1 1 40%;
    }
`

const Bio = styled.div`
    margin: 10px;

    @media (min-width: 1024px) {
        flex: 1 1 40%;
    }
`

const EmployeeModal = forwardRef(({
    closeModal,
    employee: {
        avatar,
        jobTitle,
        age,
        dateJoined,
        firstName,
        lastName,
        bio
    }}, ref) => (
    <Wrapper>
        <Content ref={ref}>
            <CloseButton onClick={closeModal} />
                <Avatar img={avatar} />
                <FullName>{firstName} {lastName}</FullName>
                <Info>
                    <h4>{jobTitle}</h4>
                    <div>{age}</div>
                    <div>{new Date(dateJoined).toLocaleDateString()}</div>
                </Info>
                <Bio>{bio}</Bio>
        </Content>
    </Wrapper>
));

export default EmployeeModal;