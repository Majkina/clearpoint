import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { getCompanyInfo } from '../../redux/actions';
import {
    getCompanyInfoFromState,
    isCompanyLoading
} from '../../redux/selectors';
import TopBar from './TopBar';

const TopBarContainer = () => {
    const dispatch = useDispatch();
    const data = useSelector(getCompanyInfoFromState);
    const isLoading = useSelector(isCompanyLoading);

    useEffect(() => {
        dispatch(getCompanyInfo());
    }, []);

    return (
        <TopBar
            data={data}
            isLoading={isLoading}
        />
    );
};

export default TopBarContainer;