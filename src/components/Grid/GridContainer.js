import React, { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import {
    getEmployees,
    openEmployeeModal,
    closeEmployeeModal
} from '../../redux/actions';
import Grid from './Grid';
import {
    getEmployeesFromState,
    isEmployeesLoading
} from '../../redux/selectors';

const GridContainer = () => {

    const dispatch = useDispatch();
    const data = useSelector(getEmployeesFromState);
    const isLoading = useSelector(isEmployeesLoading);

    useEffect(() => {
        dispatch(getEmployees());
    }, []);

    const showProfile = id => {
        dispatch(openEmployeeModal(id))
    }

    return (
        <Grid
            data={data}
            isLoading={isLoading}
            showProfile={showProfile}
        />
    );
}

export default GridContainer;