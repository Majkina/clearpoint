import React, { Fragment } from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
    background-color: lightgrey;
    border-bottom: 2px solid black;
    display: flex;
    flex-direction: column;
    justify-content: space-between;
    padding: 25px;

    @media(min-width: 768px) {
        flex-direction: row;
    }
`

const CompanyName = styled.h1`

`

const CompanyMotto = styled.h2`

`

const CompanyEst = styled.div`

`

const Left = styled.div`

`

const Right = styled.div`
    align-items: flex-end;
    display: flex;
`

const TopBar = ({ data, isLoading }) => (
    <Wrapper>
        {isLoading ?
            <div>loading...</div>
            :
            <Fragment>
                <Left>
                    <CompanyName>{data.companyName}</CompanyName>   
                    <CompanyMotto>{data.companyMotto}</CompanyMotto>
                </Left>
                <Right>
                    <CompanyEst>Since {new Date(data.companyEst).getFullYear()}</CompanyEst>
                </Right>
            </Fragment>
        }
    </Wrapper>
);

export default TopBar;