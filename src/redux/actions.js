import {
    getCompanyInfoFromApi,
    getEmployeesFromApi
} from '../clients/api';

export const SET_COMPANY_INFO = 'SET_COMPANY_INFO';
export const SET_LOADING_COMPANY_INFO = 'SET_LOADING_COMPANY_INFO';
export const ERROR_FETCHING_COMPANY_INFO = 'ERROR_FETCHING_COMPANY_INFO';

export const SET_EMPLOYEES = 'SET_EMPLOYEES';
export const SET_LOADING_EMPLOYEES = 'SET_LOADING_EMPLOYEES';
export const ERROR_FETCHING_EMPLOYEES = 'ERROR_FETCHING_EMPLOYEES';

export const OPEN_EMPLOYEE_MODAL = 'OPEN_EMPLOYEE_MODAL';
export const CLOSE_EMPLOYEE_MODAL = 'CLOSE_EMPLOYEE_MODAL';

/* COMPANY INFO */

export const getCompanyInfo = () => async dispatch => {
    dispatch(setLoadingCompanyInfo(true))

    try {
        const companyInfo = await getCompanyInfoFromApi();
        dispatch(setCompanyInfo(companyInfo))
    } catch (e) {
        dispatch(errorFetchingCompanyInfo(`Error fetching company info - ${e.message}`))
    }
}

export const setCompanyInfo = payload => ({
    type: SET_COMPANY_INFO,
    payload
});

export const setLoadingCompanyInfo = payload => ({
    type: SET_LOADING_COMPANY_INFO,
    payload
});

export const errorFetchingCompanyInfo = payload => ({
    type: ERROR_FETCHING_COMPANY_INFO,
    payload
})


/* EMPLOYEES */

export const getEmployees = () => async dispatch => {
    dispatch(setLoadingEmployees(true))

    try {
        const employees = await getEmployeesFromApi();
        dispatch(setEmployees(employees))
    } catch (e) {
        dispatch(errorFetchingEmployees(`Error fetching company info - ${e.message}`))
    }
}

export const setEmployees = payload => ({
    type: SET_EMPLOYEES,
    payload
});

export const setLoadingEmployees = payload => ({
    type: SET_LOADING_EMPLOYEES,
    payload
});

export const errorFetchingEmployees = payload => ({
    type: ERROR_FETCHING_EMPLOYEES,
    payload
});


/* MODAL */

export const openEmployeeModal = payload => ({
    type: OPEN_EMPLOYEE_MODAL,
    payload
});

export const closeEmployeeModal = () => ({
    type: CLOSE_EMPLOYEE_MODAL
});
