import {
    SET_COMPANY_INFO,
    SET_LOADING_COMPANY_INFO,
    ERROR_FETCHING_COMPANY_INFO,

    SET_EMPLOYEES,
    SET_LOADING_EMPLOYEES,
    ERROR_FETCHING_EMPLOYEES,

    OPEN_EMPLOYEE_MODAL,
    CLOSE_EMPLOYEE_MODAL
} from './actions';

const initialState = {
    companyInfo: {
        loading: false,
        errorMessage: '',
        data: {}
    },
    employees: {
        loading: false,
        errorMessage: '',
        data: []
    },
    employeeModal: {
        isOpen: false,
        id: null
    }
};

function rootReducer(state = initialState, action) {
    switch(action.type) {

        /* COMPANY */
        case SET_COMPANY_INFO:
            return {
                ...state,
                companyInfo: {
                    ...state.companyInfo,
                    loading: false,
                    data: action.payload
                }
            }

        case SET_LOADING_COMPANY_INFO: 
            return {
                ...state,
                companyInfo: {
                    ...state.companyInfo,
                    loading: true
                }
            }

        case ERROR_FETCHING_COMPANY_INFO:
            return {
                ...state, 
                companyInfo: {
                    ...state.companyInfo,
                    loading: false,
                    errorMessage: action.payload
                }
            }

        /* EMPLOYEES */
        case SET_EMPLOYEES:
            return {
                ...state,
                employees: {
                    ...state.employees,
                    loading: false,
                    data: action.payload
                }
            }


        case SET_LOADING_EMPLOYEES: 
            return {
                ...state,
                employees: {
                    ...state.employees,
                    loading: true
                }
            }

        case ERROR_FETCHING_EMPLOYEES:
            return {
                ...state, 
                employees: {
                    ...state.employees,
                    loading: false,
                    errorMessage: action.payload
                }
            }

        /* MODAL */
        case OPEN_EMPLOYEE_MODAL:
            return {
                ...state,
                employeeModal: {
                    isOpen: true,
                    id: action.payload
                }
            }

        case CLOSE_EMPLOYEE_MODAL:
            return {
                ...state,
                employeeModal: {
                    isOpen: false,
                    id: null
                }
            }

        default:
            return state;
    }
};

export default rootReducer;